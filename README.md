# Ssh Keys

Repositorio con las claves publicas correspondientes al equipo de Mikroways.

El repositorio publica en gitlab pages, nuestras claves públicas de diversas
formas, a decir:

* [Todas las claves ssh de todos los integrantes de mikroways](https://mikroways.gitlab.io/public/ssh_keys/_all.pub)
* [La clave pública de un usuario llamado car](https://mikroways.gitlab.io/public/ssh_keys/car.pub)
* [La lista de usuarios en este repositorio](https://mikroways.gitlab.io/public/ssh_keys/_users)

## Recuperar claves

Para recuperar las claves de todos los integrantes de mikroways:

```bash
curl https://mikroways.gitlab.io/public/ssh_keys/_all.pub
```

_Para su uso con ansible, se puede utilizar el módulo `get_url` directamente
contra la url_

## Uso con ansible

Este repositorio está muy relacionado al role de ansible que gestiona nuestros
usuarios, [mikroways.mw_user](https://gitlab.com/mikroways/ansible/mw-user)

## Versión anterior

Este repositorio tenía otra estructura que puede consultarse en la rama
[**legacy**](https://gitlab.com/mikroways/public/ssh_keys/-/tree/legacy)
